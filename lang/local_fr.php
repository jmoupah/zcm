<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {return;}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'envoyer_message' => 'Écrivez-nous',
	
	// I
	'info_obligatoire_02' => 'Champ nécessaire',
	'info_passe_trop_court' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_min@ minuscule(s),  @nb_maj@ majuscules(s) et @nb_int@ chiffes(s).',
	
	// L
	'label_email_subscribe' => 'Mon Email',
	
	// R
    'repondre_article' => 'Poster un commentaire',
	// S
	'succes' => 'Merci, votre message a bien été envoyé.<br />Nous vous répondons dès que possible...'
	
);