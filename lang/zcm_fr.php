<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_telecharger' => 'À télécharger',
	'a_venir' => 'À venir',
	'accesrapide_contenu' => 'Aller au contenu',
	'accesrapide_menu' => 'Menu',
	'agenda_adresse' => 'Adresse',
	'agenda_date' => 'Date',
	'agenda_evenement' => 'Événement',
	'agenda_lieu' => 'Lieu',
	'agenda_dates_a_venir' => 'Dates à venir',
	'agenda_dates_passees' => 'Dates passées',
	'archives' => 'Archives',
	'arialabel_accesrapide' => 'Accès rapide',
	'arialabel_fildariane' => 'Vous êtes ici',
	'arialabel_menulang' => 'Changer la langue',
	'arialabel_navprincipale' => 'Navigation principale',
	'aucun_evenement' => 'Pas d\'événements pour le moment',

	// E
	'epinglee' => 'Épinglée',
	'epingler' => 'Épingler',
	'epingler_explication' => 'Épingler cette actu en 1ère dans la liste des actus',	

	// F
	'fait_avec_spip' => 'Fait avec SPIP',

	// P
	'plus_d_infos' => 'Plus d\'infos',
	'publie_le' => 'Publié le',

	// V
	'voir_les_archives' => 'Voir les archives',
	'voir_tout' => 'Voir tout',
	
	// Z
	'zcm_titre' => 'Configurer ZCM',
	
);
