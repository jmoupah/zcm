<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {return;}

// Déclaration des blocs pour Z (plugin z-core)
if (!isset($GLOBALS['z_blocs'])) {
	$GLOBALS['z_blocs'] = array(
		'content',
		'head',
		'head_js',
		'header',
		'breadcrumb',
		'aside',
		'footer',
	);
}

define('_JS_ASYNC_LOAD',true);
// Ajout du champ téléphone dans Identité Extra
$GLOBALS['identite_extra'] = array('email');
