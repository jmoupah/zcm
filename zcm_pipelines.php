<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Masquer les raccourcis inutilisés dans la barre d'édition du porte plume
function zcm_porte_plume_barre_charger($barres) {
	$barre = &$barres['edition'];

	$barre->cacher('notes');
	$barre->cacher('indenter');
	$barre->cacher('desindenter');
	$barre->cacher('barre_poesie');
	$barre->cacher('guillemets');
	$barre->cacher('sepCode');
	$barre->cacher('grpCode');

	return $barres;
}