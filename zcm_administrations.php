<?php
if (!defined("_ECRIRE_INC_VERSION")) return;
 
// Inclure l'API Champs Extras
include_spip('inc/cextras');
// Inclure les champs déclarés à l'étape précédente
include_spip('base/zcm');
 
function zcm_upgrade($nom_meta_base_version,$version_cible) {
 
	$maj = array();
 
	// Création des champs extras infos_spectacle et infos_spectateurs
	cextras_api_upgrade(zcm_declarer_champs_extras(), $maj['create']);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
 
}
 
// Désinstaller proprement le plugin en supprimant les champs de la base de données
function zcm_vider_tables($nom_meta_base_version) {
	cextras_api_vider_tables(zcm_declarer_champs_extras());
	effacer_meta($nom_meta_base_version);
}