(function($){
	$(document).ready(function(){
		// menu accès rapide
		jQuery('#accesrapide a')
			.on('focus', function(){
				jQuery('#accesrapide').addClass('actif');
			})
			.on('blur', function(){
				jQuery('#accesrapide').removeClass('actif');
			});
		if (typeof window.test_accepte_ajax != "undefined") {
			test_accepte_ajax();
		}

		// Menu burger accessible
		const navBtn = $('#navprincipale button');		
		jQuery(navBtn).on('click', function(){
			$(this).attr('aria-expanded', ($(this).attr('aria-expanded') === 'false' ? 'true' : 'false'));
		})

	});
})(jQuery);
