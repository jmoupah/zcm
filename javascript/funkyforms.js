(function($){
	$(document).ready(function(){
		/* Les labels des formulaires funky / animés */
		function zcm_formulaires_funky(){
			$('.formulaire_spip input.text').focus(function() {
				$(this).parent().addClass("focused");
			});
			$('.formulaire_spip input').blur(function() {
				if($(this).val() == '') {
					$(this).parent().removeClass("focused");
				}
			});		
			$('.formulaire_spip textarea').focus(function() {
				$(this).parent().addClass("focused");
			});
			$('.formulaire_spip textarea').blur(function() {
				if($(this).val() == '' ) {
					$(this).parent().removeClass("focused");
				}			
			});
		}
		zcm_formulaires_funky();
		onAjaxLoad(zcm_formulaires_funky);
	});
})(jQuery);