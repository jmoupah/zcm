(function($){
	// Ouvrir les liens externes dans un nouvel onglet (maaaal!)
	$(document).on("click", "a.external", function() {
		window.open(this.href);
		return false;
	});
})(jQuery);