(function($){
	$(document).ready(function(){
		var $animation_elements = jQuery('.detectview');
		var $window             = jQuery(window);
		var window_height       = $window.height();
		var appearOffset        = window_height / 20;
		function detectview() {
			var window_top_position = $window.scrollTop();
			var window_bottom_position = (window_top_position + window_height);
			jQuery.each($animation_elements, function() {
				var $element = jQuery(this);
				var element_height = $element.outerHeight();
				var element_top_position = $element.offset().top + appearOffset;
				var element_bottom_position = (element_top_position + element_height);
				if ((element_bottom_position >= window_top_position) &&
					(element_top_position <= window_bottom_position)) {
					$element.addClass('detectview_inview');
				}
			});
		}
		detectview();
		$window.scroll(detectview);
	});
})(jQuery);