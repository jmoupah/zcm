<?php
 
// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) return;
 
function zcm_declarer_champs_extras($champs = array()) {

    // EPINGLER L'ACTU EN 1ER DANS LA LISTE
    $champs['spip_articles']['epingler'] = array(
        'saisie' => 'case',
        'options' => array(
            'nom' => 'epingler',
            'label' => _T('zcm:epingler'),
            'label_case' => _T('zcm:epingler_explication'),
            'valeur_oui' => 'oui',
            'restrictions' => array(
                'secteurs' => '1',
            ),
            'sql' => 'varchar(3) DEFAULT \'\' NOT NULL',
        ),
    );

    return $champs;

}